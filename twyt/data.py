# Copyright (c) 2007-2009 Andrew Price
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
# 3. The name of the author may not be used to endorse or promote products
#    derived from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
# IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
# NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
# THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

"""Data structures representing Twitter API return values"""

import sys
import time
import htmllib
import simplejson

class User:

	"""A Twitter user"""

	def __init__(self, user):

		"""
		Initialise a user. The user argument should be of the format
		returned by Twitter functions after being translated into a
		python dict.
		"""

		self.id = user[u'id']
		self.name = user[u'name']
		self.screen_name = user[u'screen_name']
		self.location = user[u'location']
		self.description = user[u'description']
		self.profile_image_url = user[u'profile_image_url']
		self.url = user[u'url']
		self.protected = user[u'protected']


class Status:

	"""A Twitter status message"""

	def __init__(self, status=None):

		"""
		Initialise the message. If status is given and is a dict
		containing the fields which Twitter returns about a status
		message, the object's fields ares populated with that data.
		"""

		if status:
			self.set(status)

	def __str__(self):

		"""
		Return a string representation of this Status message in the
		form:
		[id] name: message (date via source)
		"""

		string = ""
		try:
			# Sure takes a lot to do UTC -> local time...
			utc = time.mktime(time.strptime(
				"%s UTC" % self.created_at,
				"%a %b %d %H:%M:%S +0000 %Y %Z"))
			stamp = time.strftime("%a %b %d %H:%M:%S %Y",
					time.localtime(utc))

			string = "[%d] %s: %s (%s via %s)" % (int(self.id),
							self.user.screen_name,
							self.text,
							stamp,
							self.source)
		except AttributeError, e:
			print >>sys.stderr, e

		return string

	def set(self, status):

		"""
		Populate the object fields with data from the status argument.
		The status dict must provide the fields which Twitter returns
		in its status message structures.
		"""

		self.text = status[u'text'].replace('&gt;','>').replace('&lt;','<')
		self.created_at = status[u'created_at']
		self.id = status[u'id']

		p = htmllib.HTMLParser(None)
		p.save_bgn()
		p.feed(status[u'source'])
		self.source = p.save_end()
		if self.source.endswith("[1]"):
			self.source = self.source[:-3]

		self.truncated = status[u'truncated']
		self.user = User(status[u'user'])

	def load_json(self, s):

		"""
		Populate this object with data from JSON encoded string s
		"""

		if s:
			jsobj = simplejson.loads(s)
			self.set(jsobj)

class DirectMsg:

	"""A direct message"""

	def __init__(self, msg=None):

		"""
		Initialise the direct message. If msg is given and is a dict
		containing the fields which Twitter returns about a direct
		message, the object's fields ares populated with that data.
		"""

		if msg:
			self.set(msg)

	def __str__(self):
	
		"""
		Return a string representation of a direct message of the form:
		[id] sender -> recipient: message (date)
		"""

		string = ""
		try:
			# Sure takes a lot to do UTC -> local time...
			utc = time.mktime(time.strptime(
				"%s UTC" % self.created_at,
				"%a %b %d %H:%M:%S +0000 %Y %Z"))
			stamp = time.strftime("%a %b %d %H:%M:%S %Y",
					time.localtime(utc))

			string = "[%d] %s -> %s: %s (%s)" % (self.id,
							self.sender.name,
							self.recipient.name,
							self.text,
							stamp)
		except AttributeError, e:
			print >>sys.stderr, e
		return string

	def set(self, msg):

		"""
		Populate the object fields with data from the msg argument.
		The msg dict must provide the fields which Twitter returns
		in its direct message structures.
		"""

		p = htmllib.HTMLParser(None)
		p.save_bgn()
		p.feed(msg[u'text'])

		self.text = p.save_end()
		self.created_at = msg[u'created_at']
		self.id = msg[u'id']
		self.recipient_id = msg[u'recipient_id']
		self.recipient = User(msg[u'recipient'])
		self.recipient_screen_name = msg[u'recipient_screen_name']
		self.sender_id = msg[u'sender_id']
		self.sender = User(msg[u'sender'])
		self.sender_screen_name = msg[u'sender_screen_name']

	def load_json(self, s):

		"""Populate this object with data from JSON encoded string s"""

		if s:
			jsobj = simplejson.loads(s)
			self.set(jsobj)


class StatusList(list):

	"""A list of status messages"""

	def __init__(self, json=None):

		"""
		Initialise a list of status messages. If the json parameter is
		given and is a JSON encoded string containing a list of status
		messages encoded as JSON, the list will be populated with
		Status objects.
		"""

		self.lastts = ""
		self.lastid = 0
		if json is not None:
			try:
				self.jsobj = simplejson.loads(json)
				if type(self.jsobj) == dict and \
						self.jsobj.has_key('error'):
					raise Exception(self.jsobj['error'])
			except ValueError:
				self.jsobj = []

			for i in self.jsobj:
				self.append(Status(i))
			if len(self) > 0:
				self.lastts = self[0].created_at
				self.lastid = self[0].id

	def get_last_time(self):

		"""
		Returns the most recent timestamp from the list of status
		messages.
		"""

		return self.lastts
	
	def get_last_id(self):

		"""
		Returns the id of most recent status message in the list
		"""

		return self.lastid


class DirectList(list):

	"""A list of direct messages"""

	def __init__(self, json=None):

		"""
		Initialise a list of direct messages. If the json parameter is
		given and is a JSON encoded string containing a list of direct
		messages encoded as JSON, the list will be populated with
		DirectMsg objects.
		"""

		if json is not None:
			try:
				self.jsobj = simplejson.loads(json)
			except ValueError:
				self.jsobj = []

			for i in self.jsobj:
				self.append(DirectMsg(i))

class RateLimit(dict):
	"""
	Rate limit status
	"""
	def __init__(self, json=None):
		"""
		Initialise the object. If json is given and is a json string
		containing the fields which Twitter returns about a direct
		message, the object's fields ares populated with that data.
		"""
		if json:
			self.load_json(json)

	def __str__(self):
		"""
		Return a string representation of the rate limit.
		"""
		map = [ ("reset_time", "Reset time: "),
			("reset_time_in_seconds", "Reset time (secs): "),
			("remaining_hits", "Remaining requests: "),
			("hourly_limit", "Hourly limit: ")]
		string = ""
		try:
			for i in map:
				string += "%s%s\n" % (i[1], self[i[0]])
		except KeyError, e:
			print >>sys.stderr, e
		return string

	def load_json(self, s):
		"""
		Populate this object with data from JSON encoded string s
		"""
		if s:
			jsobj = simplejson.loads(s)
			for key in jsobj.keys():
				self[key] = jsobj[key]

